<!DOCTYPE html>
<html>

<head>
    <title>Margo-Jenkins</title>
</head>

<body bgcolor="yellow">
    <h1>NEVER GIVE UP!</h1>
    <h2>Welcome to Java Maven Application</h2>
    <div class="col-lg-15 mx-auto p-4 py-md-3">
        <div class="d-flex align-items-center pb-3 mb-5 border-bottom"></div>
        <div class="row">
            <div class="col-9" style="padding-left: 100px;">
                <h1 style="color: rgb(173, 18, 18);">Jenkins-task</h1>
                <h1 style="color: rgb(61, 62, 62);">
                    <p class="fs-14 col-md-15"> <b>CICD pipeline via Jenkins</b></p>
                </h1>
                <div class="row">
                    <div class="col-md-12">
                        <p><b>CI:</b></p>
                        <ul class="icon-list">
                            <li>Clone the repository</li>
                            <li>Launch static code analysis, Bugs, Vulnerabilities, Security Hotspots, Code Smells
                                available on the SonarQube server</li>
                            <li>Builds a Docker image</li>
                            <li>Tag the image 2 times (latest and build version)</li>
                            <li>Push the image to the Docker Hub</li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <p><b>CD:</b></p>
                        <ul class="icon-list">
                            <li>Options at startup: a. Env name (dev/qa) b. Version number (i.e. image tag, version, or
                                latest).</li>
                            <li>Deploy the image to the selected env</li>
                            <li>Healthcheck status of the deployed env</li>
                            <li>Notifications</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>