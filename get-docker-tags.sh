#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <image_name>"
    exit 1
fi

IMAGE_NAME=$1

# Use Docker Hub API to get information about tags
TAGS=$(curl -s "https://registry.hub.docker.com/v2/repositories/${IMAGE_NAME}/tags/" | jq -r '.results|.[]|.name')

if [ -z "$TAGS" ]; then
    echo "Failed to retrieve tags for the image ${IMAGE_NAME}. Maybe the image name is incorrect or the image does not exist."
    exit 1
fi

# Return tags in a format that can be used in Jenkins
echo "$TAGS"
