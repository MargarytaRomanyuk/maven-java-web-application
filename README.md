<center>

# java-maven webapp for EPAM Jenkins-task

</center>

- In the dir `src` is the java-maven project code, which I cloned from https://github.com/denofprogramming/maven-java-web-application
- I make some changes in index.jsp file for visually show the changes in the app during the CI/CD proccess demonstration.

- The description of the task and the solution are available at the link [Jenkins-task](https://gitlab.com/devops-7-romaniuk-marharyta/devops-7-romaniuk-marharyta-2/-/tree/jenkins/Jenkins?ref_type=heads)

